const User = require("../models/user") 
const formidable = require("formidable")
const fs = require("fs")
var validator = require("email-validator");

exports.creatuser = (req, res)=>{
    let form = new formidable.IncomingForm();
    form.keepExtensions= true;
    
    form.parse(req, (err, fields, file)=>{
        if(err)
        {
            return res.status(400).json({
            error:"there is some problem with image"
            });
        }
        //todo: restrictions on field

        let user = new User(fields);

        
        
        //handle file here
        if(file.photo){

            if (file.photo.size > 3000000){
                return res.status(400).json({
                    error:"file is big"
                });
            }
            user.photo.data = fs.readFileSync(file.photo.path);
            user.photo.contentType = file.photo.type;
        }
        //save to db and email format check
        if(!(validator.validate(user.email))){
            return res.status(400).json({
                error:"email format is invalid"
            });
        }
        else{

        user.save((req, user)=>{
            if(err){
                req.status(400).json({
                    error:"saving in bd "
                })
            }
            res.json({
                Name:user.name,
                Lastname:user.lastname,
                Email:user.email,
                file_type:user.photo.contentType
            });
        })
      
    }
    });

 
};

