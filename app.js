const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const userroutes = require("./routes/user")

//connect to db
mongoose
    .connect('mongodb+srv://sam:thesam@cluster0.j9hke.mongodb.net/test?retryWrites=true&w=majority', 
        {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex:true
            
    })
    .then(()=>{
        console.log("db connected")
    });


app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
    


//my routes
app.use("/api",userroutes);

//port

var port=5000;


//starting server 
app.listen(port, ()=>{
    console.log(`app is running at ${port}`); })
